﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Rainfall">
// Copyright (c) Rainfall. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>
//-----------------------------------------------------------------------
namespace Stationeers_Map_Trimmer_CLI
{
    using System;
    using NDesk.Options;
    using StationeerUtilities;

    public class Program
    {
        public static void Main(string[] args)
        {
            string mapPath = string.Empty;
            int xCoord = 0, zCoord = 0, radius = 0;

            OptionSet set = new OptionSet()
            {
                { "m=|mapFile=", "The full path to the world.bin file", v => { mapPath = v; } },
                { "x=", "X coordinate of trim center point", v => { xCoord = int.Parse(v); } },
                { "z=", "Z coordinate of trim center point", v => { zCoord = int.Parse(v); } },
                { "r=|radius=", "X coordinate of trim center point", v => { radius = int.Parse(v); } }
            };
            System.IO.FileInfo mapFile = null;

            try
            {
                set.Parse(args);
                mapFile = new System.IO.FileInfo(mapPath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid arguments: " + e.Message);
                set.WriteOptionDescriptions(Console.Out);
            }
            if (mapFile == null || !mapFile.Exists)
            {
                if (mapPath.Length > 0)
                {
                    Console.WriteLine(string.Format("File {0} does not exist", mapPath));
                }
                Environment.Exit(0);
            }

            var a = new Saves();
            a.LoadSave(mapFile.FullName);
            a.TrimRadius(radius, xCoord, zCoord);

            string outPath = System.IO.Path.Combine(mapFile.Directory.FullName, "world.bin.trimmed");

            a.Save(outPath);

            Console.WriteLine("Map has been trimmed and saved");
        }
    }
}

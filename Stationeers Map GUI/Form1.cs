﻿//-----------------------------------------------------------------------
// <copyright file="Form1.cs" company="Rainfall">
// Copyright (c) Rainfall. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>
//-----------------------------------------------------------------------
using StationeerUtilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Stationeers_Map_GUI
{
    public partial class Form1 : Form
    {
        StationeerUtilities.Saves currentSave;
        System.Drawing.Image currentMap;
        System.Drawing.Image workingImage;

        Vector3 centerPoint;
        //float xScale, yScale;

        public Form1()
        {
            InitializeComponent();
            currentSave = new StationeerUtilities.Saves();

            //pictureBox1.MouseUp += PictureBox1_MouseUp;
        }

        private void PictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            updateMap(e.X, e.Y, (int)numericTrimRadius.Value);
            setCenterPoints(e.X, e.Y);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog odlg = new OpenFileDialog();
            if(odlg.ShowDialog() == DialogResult.OK)
            {
                currentSave.LoadSave(odlg.FileName);
                generateMap();
                updateMap(0,0,0);
            }
        }

        public void generateMap()
        {
            currentMap = currentSave.DrawMap();
        }

        private void btnSaveMap_Click(object sender, EventArgs e)
        {
            SaveFileDialog sdlg = new SaveFileDialog();
            if (sdlg.ShowDialog() == DialogResult.OK)
            {
                currentMap.Save(sdlg.FileName);
            }
        }

        public void setCenterPoints(int centerX, int centerZ)
        {
            Vector3[] mapBounds = currentSave.GetBounds();
            Vector3 mapDimensions = currentSave.GetDimensions();

            centerPoint = new Vector3();
            float xRatio = (float)centerX / workingImage.Width;
            float zRatio = (float)centerZ / workingImage.Height;
            centerPoint.x = (mapBounds[0].x + (mapDimensions.x*xRatio))/currentSave.ChunkSize;
            centerPoint.z = (mapBounds[0].z + (mapDimensions.z * zRatio))/currentSave.ChunkSize;

            this.centerPointX.Text = Convert.ToString(centerPoint.x);
            this.centerPointZ.Text = Convert.ToString(centerPoint.z);
        }

        private void btnTrim_Click(object sender, EventArgs e)
        {
            if (numericTrimRadius.Value == 0) return;
            currentSave.TrimRadius((int)numericTrimRadius.Value, int.Parse(centerPointX.Text), int.Parse(centerPointZ.Text));
            generateMap();
            updateMap(1, 1, 1);
        }

        public void updateMap(int centerX, int centerY, int radius)
        {
            if (centerX > -1 && currentMap != null)
            {
                float scaleFactor;
                if(currentMap.Height > currentMap.Width)
                {
                    scaleFactor = (float)pictureBox1.Height / currentMap.Height;
                }
                else
                {
                    scaleFactor = (float)pictureBox1.Width / currentMap.Width;
                }

                float newXDimension = currentMap.Width* scaleFactor;
                float newYDimension = currentMap.Height * scaleFactor;

                var destRect = new Rectangle(0, 0, (int)Math.Floor(newXDimension), (int)Math.Floor(newYDimension));
                workingImage = new Bitmap((int)Math.Floor(newXDimension), (int)Math.Floor(newYDimension));
                using (var graphics = System.Drawing.Graphics.FromImage(workingImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var wrapMode = new ImageAttributes())
                    {
                        wrapMode.SetWrapMode(WrapMode.Clamp);
                        graphics.DrawImage(currentMap, destRect, 0, 0, currentMap.Width, currentMap.Height, GraphicsUnit.Pixel);
                    }

                    float scaledRadius = radius * scaleFactor;
                    Pen pen = new Pen(Color.Red);
                    Rectangle rect = new Rectangle(new Point(centerX - (int)scaledRadius, centerY - (int)scaledRadius), new Size((int)(scaledRadius*2), (int)(scaledRadius*2)));
                    graphics.DrawEllipse(pen, rect);
                }

                pictureBox1.BackgroundImage = workingImage;
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sdlg = new SaveFileDialog();
            if (sdlg.ShowDialog() == DialogResult.OK)
            {
                currentSave.Save(sdlg.FileName);
            }
        }
    }
}
